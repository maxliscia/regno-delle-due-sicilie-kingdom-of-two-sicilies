# Regno delle Due Sicilie (Kingdom of Two Sicilies)

## Ver. 4
-EN- Social policies costs now 50% less.
-EN- Cities produce culture only after Archeology discover
-IT- Le politiche sociali costano adesso il 50% in meno di cultura
-IT- Le citt� producono cultura addizionale soltanto dopo la scoperta dell'Archeologia

## Ver. 3
-EN- Social policies costs now 25% less.
-EN- Cities produce an additional +1 culture after Archeology discover
-IT- Le politiche sociali costano adesso il 25% in meno di cultura
-IT- Le citt� producono un +1 di cultura addizionale dopo la scoperta dell'Archeologia

## Ver. 2
-EN- The Pizzeria now provides +2 happiness (was +3). 
-EN- Bonuses to the trade routes of the pizzeria were deleted. 
-EN- The Pizzeria provides +1 Pizza, a new luxury resource exclusive of this civilization.
-IT- La Pizzeria fornisce ora +2 felicit� (prima era +3).
-IT- Eliminati i bonus alle rotte commerciali della pizzeria.
-IT- La Pizzeria fornisce +1 Pizza, una nuova risorsa di lusso esclusiva di questa civilt�.

## Ver. 1 Stable
-EN- Correct the amount of gold bonus for sea trade routes of Pizzeria;
-EN- Add +1 culture to Villa Comunale;
-EN- Changed culture for new social policies to -20%.
-IT- Corretta la quantit� d'oro bonus per le rotte commerciali marine della Pizzeria;
-IT- Aggiunto +1 cultura alla Villa Comunale;
-IT- Cambiato il modificatore della cultura per le nuove politiche sociali a -20%.

## Ver. 1 - Beta
-EN- First release. In the test phase.
-IT- Prima versione. In fase di test.
