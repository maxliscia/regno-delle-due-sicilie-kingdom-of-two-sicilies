# Regno delle Due Sicilie (Kingdom of Two Sicilies)
## Ver. 4
### Mod for Civilization V - Brave New World

(Italiano sotto l'inglese) 
-- ENGLISH -- 
Adds the Kingdom of Two Sicilies to the base game.

~~~ 
Note: 
- This civilization requires the Brave New World expansion. 
- Updates to the Mod will most likely break save games.
  Please do not update until you are ready to begin a new game. 
~~~ 

Leader: Ferdinando II di Borbone 
UA: Social progress: Cities produce +2 culture (after Archeology) and +1 gold. Social policies cost 50% less. 
UB: Villa Comunale: Replaces Garden. In addition to the bonus of the garden (+25% Great People) Also provides +1 happiness and +1 culture. Does'nt requires water. 
UB: Pizzeria: Replace Zoo. Provides +2 happiness, +2 food and +2 culture. It also provides +1 Pizza, exclusive luxury resource of this civilization. 

--ITALIANO -- 
Aggiunge il Regno delle Due Sicilie al gioco.
 
~~~ 
Nota: 
- Questo mod richiede Brave New World. 
- Aggiornare questo mod può molto probabilmente danneggiare la partita salvata.
  Non aggiornare finchè non sei pronto a cominciare una partita nuova. 
~~~

Leader: Ferdinando II di Borbone 
UA: Le città producono +2 cultura (dopo la scoperta dell'Archeologia) e +1 oro. Le politiche sociali costano il 50% in meno. 
UB: Villa Comunale: Sostituisce il Giardino. Oltre al bonus del Giardino (+25% generazione Grandi Personaggi) fornisce anche +1 felicità e +1 cultura. Non necessita del fiume o del lago per essere costruita. 
UB: Pizzeria: Sostituisce lo Zoo. Fornisce +2 felicità, +2 cibo e +2 cultura. Inoltre fornisce anche +1 Pizza, risorsa di lusso esclusiva di questa civiltà.